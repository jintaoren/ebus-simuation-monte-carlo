import numpy as np
import scipy as sp
import pandas as pd

import matplotlib.pyplot as plt
import matplotlib as mpl
import sklearn
import os 
import sys
#import seaborn as sns
import random
import names

import time 
import datetime
from bus_simulation.Measure import *
from bus_simulation.Simulation import *


def passenger_demand(demand, sigma , start, end):
    """
    Make two distrubution of passengers, with guassian distribution
    """
    #np.random.seed(42) # force repeatable random seeds
    
    mu1 = 10*3600
    mu2 = 17*3600

    n1 = np.random.normal(mu1, sigma, int(demand/2))
    n2 = np.random.normal(mu2, sigma, int(demand/2))

    passenger_dist = np.round(np.hstack((n1,n2)))
    for i in range(len(passenger_dist)):
        if passenger_dist[i] < start or passenger_dist[i] >= end:
            passenger_dist[i] = int(start + (np.random.random() * (end - start)))
            
    s = passenger_dist/3600
    count, bins, ignored = plt.hist(s, 60, density=False)
    plt.title("Passenger demand with two cluster peak hours, n={}".format(demand))
    plt.xlabel("time(h), mu1={}, mu2={}, sigma = {}".format(mu1/3600,mu2/3600,sigma))
    plt.ylabel("passenger Number")
    plt.savefig('passenger_dist.png', dpi = 300)
    return passenger_dist

def passenger_demand_noplot(demand, sigma , start, end):
    """
    Make two distrubution of passengers, with guassian distribution
    """
    #np.random.seed(42) # force repeatable random seeds
    
    mu1 = 10*3600
    mu2 = 17*3600

    n1 = np.random.normal(mu1, sigma, int(demand/2))
    n2 = np.random.normal(mu2, sigma, int(demand/2))

    passenger_dist = np.round(np.hstack((n1,n2)))
    for i in range(len(passenger_dist)):
        if passenger_dist[i] < start or passenger_dist[i] >= end:
            passenger_dist[i] = int(start + (np.random.random() * (end - start)))

    return passenger_dist

def Sim_measure(interval=[11, 20, 15, 10, 20] , dep_time = [5, 30, 20, 25, 6]):
    passenger_dist = passenger_demand(demand=2000, sigma = 4500, start = 8*3600, end=20*3600)
    simulator = Simulation(passenger_dist, bus_start_interval = 5, gen_bus_interval= interval, dep_time = dep_time )
    simulator.run()
    measure = Measure(simulator)
    return measure.avg_travel_time(),measure.avg_wait_time(), measure.sum_energy_cost()


TTmean, WTmean, Energy = Sim_measure(interval=[5, 20, 15, 10, 20] , dep_time = [5, 4, 10, 7, 6])