import numpy as np
import scipy as sp
import pandas as pd

import matplotlib.pyplot as plt
import matplotlib as mpl
import sklearn
import os 
import sys
#import seaborn as sns
import random
import names

random.seed(20)

import time 
import datetime 


from .Bus import *
from .Route import *
from .Passenger import *
from .Station import *
from .Simulation import *

from .tools import *

from itertools import chain

class Measure:
    def __init__(self, simulator):
        self.simulator = simulator
        
        self.passengers = []
        self.average_wait_times = []
        self.travel_times = []

        self.total_bus = list(chain(*[r.bus_list for r in self.simulator.route_list]))
        self.station_passengers =  list(chain(*[s.passengers for s in self.simulator.bus_stations]))
        self.bus_passengers =  list(chain(*[b.passengers for b in self.total_bus]))
        self.walk_passengers = self.simulator.walk_list
        self.arrival_passengers = self.simulator.arrivals
        
        self.passengers = self.total_passenger()
        
        
    def total_passenger(self):
        total_pass = [] 
        total_pass.extend(self.station_passengers)
        total_pass.extend(self.bus_passengers )
        total_pass.extend(self.walk_passengers) 
        total_pass.extend(self.arrival_passengers)
        
        #print('There are total {} passengers in simulation, {} choose to walk'
        #      ', {} took bus and arrived, {} still on bus, {} still waiting at station'.format(
        #      len(total_pass), 
        #      len(self.walk_passengers), 
        #      len(self.arrival_passengers),
        #      len(self.bus_passengers),
        #      len(self.station_passengers)
        #      ))
        return total_pass
    
    def travel_time(self):
        return [p.travel_time() for p in self.arrival_passengers]
        
    def avg_travel_time(self):
        return int(np.mean(self.travel_time()))
    
    def wait_time(self):
        return [p.time_waited_for_bus for p in self.passengers]
    
    def avg_wait_time(self):
        return int(np.mean(self.wait_time()))
    
    def sum_energy_cost(self):
        return np.sum([b.total_energy_cost for b in self.total_bus])
    
