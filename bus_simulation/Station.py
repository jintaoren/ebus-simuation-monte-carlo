import numpy as np
import scipy as sp
import pandas as pd

import matplotlib.pyplot as plt
import matplotlib as mpl
import sklearn
import os 
import sys
#import seaborn as sns
import random
import names

random.seed(20)

import time 
import datetime 

from .Bus import *
from .Route import *
from .Passenger import *
from .tools import *


class Station:
    def __init__(self,  index = ''):
        self.passengers = []
        self.index = index
        self.arrival_list = [] 
        self.last_dep_time = None
        
        
    def enqueue(self,cur_time, destination):
        """
        When a passenger arrives, add him/her to the queue.
        This function is the function to generate passenger objects.
        
        """
        passenger = Passenger(origin=self.index,
                              destination=destination,
                              start_time=cur_time)
        self.passengers.append(passenger)
        return passenger
        
    def dequeue_to_bus(self, cur_time, bus):
        """
        When a bus comes, check if the passenger's destination on the bus' not arrived station list,
        following a first come first serve rule to get on bus. Put the passengers who want
        to get on the bus in a pop(wait) list. Calculate the bus capacity and pop list to check how many
        passengers to pop. In another word, pop passengers to bus following the minumum of bus capacity or 
        wait list length.
        
        Arguments
        ---------
        cur_time: float
        pop_list: list of passenger index in station who want to get on the bus. 
        bus: the bus arrived.
        
        Returns
        -------
        nb_to_bus: number of passengers get on the bus.
        """
        
        self.last_dep_time = cur_time
        
        if len(self.passengers) < 1:
            return None

        pop_list = []
        for i in range(len(self.passengers)):
            if self.passengers[i].destination in bus.route[bus.pos+1:]:
                pop_list.append(i)   
                
        nb_to_bus = min(bus.free_seats, len(pop_list))  
        pprint(cur_time, 'Station {} receives bus {}: {} people want get on,'
               ' {} available seats'.format(self.index, bus.index, len(pop_list), bus.free_seats))
        if len(pop_list ) == 0:
            pprint(cur_time,'non poplist to onboarding...')
            return 0
        
        for i in range(nb_to_bus):
            passenger = self.passengers.pop(pop_list[0])
            pop_list.pop(0)
            pop_list = [x - 1 for x in pop_list]
            passenger.time_waited_for_bus = cur_time - passenger.start_time 
            passenger.bus_index = bus.index
            bus.take_in(cur_time,passenger)
            
         
        return nb_to_bus
    
    def dequeue_to_walk(self, cur_time, max_wt):
        """
        Dequeue passengers whose waiting time exceeds the maximum wait time threshold. Adds them to a walk list.
        
        Arguments
        ---------
        cur_time: float
        pop_list: list of passenger index in station whose waiting time exceeds the maximum wait time threshold.
        walk_list: list of passenger who choose to to walk globally.
        
        Returns
        -------
        walk_list: updated list of passenger who choose to to walk globally.
        """
        walk_list = []
        pop_list = []
        
        
        for i in range(len(self.passengers)):
            if (cur_time - self.passengers[i].start_time) > max_wt:
                pop_list.append(i)
                self.passengers[i].time_waited_for_bus = cur_time - self.passengers[i].start_time
                self.passengers[i].end_time = cur_time
                self.passengers[i].walk = True
                walk_list.append(self.passengers[i])
        
        self.passengers = [p for i,p in enumerate(self.passengers) if i not in pop_list]
        
        
        #pprint(cur_time, '*At Station {}, {} people feel '
        #       'unsatisfied of the sevice and choose to walk. {} people still waiting'.format(self.index,
        #                                                len(pop_list), len(self.passengers)))
        return walk_list
            
    def size(self):
        """returns the station's waiting length"""
        return len(self.passengers) 

    