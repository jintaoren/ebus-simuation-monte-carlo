import numpy as np
import scipy as sp
import pandas as pd

import matplotlib.pyplot as plt
import matplotlib as mpl
import sklearn
import os 
import sys
#import seaborn as sns
import random
import names

random.seed(20)

import time 
import datetime 

from .Bus import *
from .Passenger import *
from .Station import *

from .Passenger import *

from .tools import *


# Bus class
class Route:
    def __init__(self, route_num):
        self.route_num = route_num
        self.route_list = [['A', 'E', 'F'], ['A', 'B', 'C'] , ['A', 'D'], ['B', 'E', 'F'] , ['D', 'E', 'F'] ]
        self.route = self.route_list[self.route_num] #inital route by givn route number 0-4
        self.route_bus_limit = [3,3,2,2,2]
                
        self.bus_list = []
        self.time_unit = 60
        self.station_dist_graph = {'A':{'B':10*self.time_unit, 'E':25*self.time_unit, 'D':35*self.time_unit},
                                   'B':{'A':10*self.time_unit, 'C':15*self.time_unit, 'E':15*self.time_unit},
                                   'C':{'B':15*self.time_unit},
                                   'D':{'A':35*self.time_unit, 'E':15*self.time_unit},
                                   'E':{'A':25*self.time_unit, 'B':15*self.time_unit, 
                                        'D':15*self.time_unit, 'F':15*self.time_unit},
                                   'F':{'E':15*self.time_unit}}
        
        
        
    def calculate_all_dist(self):
        """
        calculate distances by adding all the distances between each node(station).
        
        currently, distance represents time(minutes).
        """
        dist = 0 
        for v, w in zip(self.route[:-1],self.route[1:]):
            dist += self.station_dist_graph[v][w]
        return dist
    
    def calculate_two_dist(self, station1, station2):
        """calcualte distance/time between two stops"""
        dist = self.station_dist_graph[station1][station2]
        return dist
    
    
    def run_all_bus(self):
        """peform bus operations in this route"""
        for bus in self.bus_list:
            bus.to_operate()
    
    def generate_bus(self):
        if  len(self.bus_list) >= self.route_bus_limit[self.route_num]:
            print("failed to add bus due to route {} bus number limitation {}...".
                  format(self.route_num+1, self.route_bus_limit[self.route_num]))
            return 0
        else: 
            bus_index = (self.route_num+1)*10 + len(self.bus_list)+1
            bus = Bus(index=bus_index,
                      route_obj=self,
                      route=self.route)
            self.bus_list.append(bus)
            #print("bus {} route {} created.".format(bus.index, bus.route))
            return 1
    