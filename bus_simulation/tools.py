

import numpy as np
import scipy as sp
import pandas as pd

import matplotlib.pyplot as plt
import matplotlib as mpl
import sklearn
import os 
import sys
#import seaborn as sns
import random
import names

import time 
import datetime 


def time_str_to_seconds(time_str):
    x = time.strptime(time_str.split(',')[0],'%H:%M:%S')
    return datetime.timedelta(hours=x.tm_hour,minutes=x.tm_min,seconds=x.tm_sec).total_seconds()

def time_seconds_to_str(cur_time):
    str_time = str(datetime.timedelta(seconds=cur_time))
    #print(str_time)
    return str_time

def pprint(cur_time, strs, ending = False):#
    """just a print function with time"""
    if ending:
        print('[{}]: {}'.format(time_seconds_to_str(cur_time),strs), end = '')
    else:
        print('[{}]: {}'.format(time_seconds_to_str(cur_time),strs))


def passenger_demand(demand, sigma , start, end):
    """
    Make two distrubution of passengers, with guassian distribution
    """
    #np.random.seed(42) # force repeatable random seeds
    
    mu1 = 10*3600
    mu2 = 17*3600

    n1 = np.random.normal(mu1, sigma, int(demand/2))
    n2 = np.random.normal(mu2, sigma, int(demand/2))

    passenger_dist = np.round(np.hstack((n1,n2)))
    for i in range(len(passenger_dist)):
        if passenger_dist[i] < start or passenger_dist[i] >= end:
            passenger_dist[i] = int(start + (np.random.random() * (end - start)))
            
    return passenger_dist
