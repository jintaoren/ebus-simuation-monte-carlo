import numpy as np
import scipy as sp
import pandas as pd

import matplotlib.pyplot as plt
import matplotlib as mpl
import sklearn
import os 
import sys
#import seaborn as sns
import random
import names

random.seed(20)

import time 
import datetime 

from .Bus import *
from .Route import *
from .Station import *

from .tools import *

class Passenger:
    """
    Passenger class is a object for storing passenger basic information inlcuding
    the waiting time, 
    """
    def __init__(self, origin, destination, start_time):
        self.origin = origin
        self.destination = destination
        self.start_time = start_time
        self.end_time = None
        self.time_waited_for_bus = 0
        self.bus_index = None
        self.get_name = self.get_name()
        self.name = self.get_name
        
        self.walk = False
        
        self.time_unit = 60*4
        self.wak_dist_graph = {'A':{'B':10*self.time_unit, 'E':25*self.time_unit, 'D':35*self.time_unit},
                                   'B':{'A':10*self.time_unit, 'C':15*self.time_unit, 'E':15*self.time_unit},
                                   'C':{'B':15*self.time_unit},
                                   'D':{'A':35*self.time_unit, 'E':15*self.time_unit},
                                   'E':{'A':25*self.time_unit, 'B':15*self.time_unit, 
                                        'D':15*self.time_unit, 'F':15*self.time_unit},
                                   'F':{'E':15*self.time_unit}}
         
        
    def get_name(self):
        return names.get_full_name()
    
    def travel_time(self):
        if self.walk:
            return station_dist_graph[self.origin][self.destination] + self.end_time
        else:
            return self.end_time - self.start_time
    
    """        
    def wait_time(self, cur_time):
        if self.location == 0:
            return  cur_time - self.dep_time
        else:
            raise "warning pasenger not at station!"
    def boarding(self, bus):
        if self.location != 0 :   
            raise "warning pasenger not at station!"
        else: 
            self.location = 1
            self.bus = bus
    def status():
        print('Passenger')
    """
        