import numpy as np
import scipy as sp
import pandas as pd

import matplotlib.pyplot as plt
import matplotlib as mpl
import sklearn
import os 
import sys
#import seaborn as sns
import random
import names

random.seed(20)

import time 
import datetime 


from .Station import *


def time_str_to_seconds(time_str):
    x = time.strptime(time_str.split(',')[0],'%H:%M:%S')
    return datetime.timedelta(hours=x.tm_hour,minutes=x.tm_min,seconds=x.tm_sec).total_seconds()

def time_seconds_to_str(cur_time):
    str_time = str(datetime.timedelta(seconds=cur_time))
    #print(str_time)
    return str_time

def pprint(cur_time, strs, ending = False):#
    """just a print function with time"""
    if ending:
        print('[{}]: {}'.format(time_seconds_to_str(cur_time),strs), end = '')
    else:
        print('[{}]: {}'.format(time_seconds_to_str(cur_time),strs))
from IPython.core.debugger import set_trace


class Bus:
    
    seats = 15
    def __init__(self, 
                 index,
                 route_obj,
                 route,
                 max_battery=100000, # wh
                 size=15,
                 pos=0.0,
                 consume_rate = 0.082, #inintial consume rate per minute
                 charge_speed = 8): #
        
        self.index = index
        self.route_obj = route_obj
        self.route = route
        self.pos = 0
        self.size = size
        
        self.operate = False 
        self.charge_complete = True
        self.init_consume_rate = consume_rate
        self.charge_speed = charge_speed
        
        self.energy_cost = 0
        
        
        self.run_time = 0

        
        self.passengers = []
        self.total_passengers = 0
        
        self.max_battery = max_battery
        self.current_battery = max_battery
        self.total_energy_cost = 0
        
        
        self.stop = False
        
        
        
    @property
    def free_seats(self):
        return self.size - len(self.passengers)
    
    @property
    def consume_rate(self):
        consume_list = [0.0820,0.3210,0.3260,0.3300,0.3350,0.3390,0.3430,
         0.3480,0.3520,0.3570,0.3610,0.3650,0.3700,0.3740,0.3790,0.383]
        np = len(self.passengers)
        return consume_list[np] #change to per second
    
    
    def take_in(self, cur_time, passengers):
        """take in passengers"""
        len_pass_before = len(self.passengers)
        self.passengers.append(passengers)
        self.total_passengers += 1
        
        return len(self.passengers)
            
    def get_off(self, station_index, cur_time):
        """
        Passenger arrives destination and leaves the bus.
        """
        len_pass_before = len(self.passengers)
        out_passengers = [passenger for passenger in self.passengers
                          if passenger.destination == station_index]

        for passenger in out_passengers:
            passenger.end_time = cur_time
            self.passengers.remove(passenger)
            
        pprint(cur_time, "Bus {} at {} having {}/{} passengers deboarding".format(self.index, 
                                                                               self.route[self.pos], 
                                                                               len(out_passengers),
                                                                               len_pass_before))
        return out_passengers  
        
            
    def terminal(self, cur_time, stop_time):
        """
        This function is called when the bus reaches the last stop.  All the
        passengers get out of the bus.
        Returns
        -------
        """
        self.passengers = []
        temp_route = self.route.copy()
        temp_route.reverse()

        self.route = temp_route.copy() #reverse route 
        pprint(cur_time, "Bus {} is going to run a reversed route {}".format(self.index, self.route))     

        self.run_time =0
        self.pos = 0
        self.operate = False
        self.to_stop(cur_time, stop_time)
    
        return True
        
    def is_battery_enough(self):
        """
        This is a function to check if the bus' battery is enough for the next
        run. Determined by consume_rate*route_distance and battery left.
        """
        distance =  self.route_obj.calculate_all_dist()
        if self.consume_rate * distance < self.current_battery:
            return True
        else:
            return False
        
    def charge(self):
        """
        simulate charging battery
        
        """
        if self.current_battery < self.max_battery:
            self.current_battery += self.charge_speed   #battery unit/sec
            return False
        else:
            return True
        
        
    def to_operate(self,cur_time, stop_time, bus_station_list):
        """To opereate a bus"""
        
        if not self.stop: #if the stop sign is False

            if not self.operate:
                #print(self.charge_complete, self.pos)
                if self.charge_complete and self.pos==0:
                    #starting from terminals with full battery, take in all passengers
                    for station in bus_station_list:
                            if station.index == self.route[self.pos]:
                                station.dequeue_to_bus(cur_time, self) 
                    self.operate = True


            if self.route[self.pos]== self.route[0]:
                if not self.is_battery_enough():
                    self.operate = False
                    self.charge_complete = self.charge()
                    if cur_time % 60 ==0:
                        pprint(cur_time, "Bus {} is charging at station {}".format(self.index, self.route[0]))

                elif self.current_battery >= self.max_battery and not self.operate :
                    self.operate = True
                    pprint(cur_time, "Bus {} charge complete at station {}".format(self.index, self.route[0]))

                elif not self.charge_complete:
                    self.charge_complete = self.charge()
                    if cur_time % 60 ==0:
                        pprint(cur_time, "Bus {} is charging at station {}".format(self.index, self.route[0]))

            if self.operate == True:
                self.run_time +=1
                self.current_battery-= self.consume_rate
                self.total_energy_cost += self.consume_rate
                
                if len(self.route) == 2: 

                    #print(self.route_obj.calculate_two_dist(self.route[0], self.route[1]), self.run_time)

                    if self.route_obj.calculate_two_dist(self.route[0], self.route[1]) == self.run_time:
                        self.run_time = 0
                        #print("Bus {} reaches a terminal {}".format(self.index, self.route[1] ))
                        self.pos += 1 
                        pprint(cur_time, "Bus {} arrives at terminal {}".
                               format(self.index, self.route[self.pos]))
                        for station in bus_station_list:
                            if station.index == self.route[self.pos]:
                                pprint(cur_time, "Bus {}  arrives at stop {}".format(self.index, 
                                                                                     self.route[self.pos]))
                                get_off_passengers = self.get_off(station.index, cur_time)
                                station.arrival_list.extend(get_off_passengers)
                                self.terminal(cur_time, stop_time)



                elif len(self.route) > 2: 

                    if self.route_obj.calculate_two_dist(self.route[self.pos], 
                                                          self.route[self.pos+1]) == self.run_time:
                        self.pos += 1
                        self.run_time = 0
                        #print("*bus {} is at postion {}".format(self.index, self.pos))
                        for station in bus_station_list:
                            if station.index == self.route[self.pos]:
                                pprint(cur_time, "Bus {} arrives at stop {}".format(self.index, 
                                                                                     self.route[self.pos]))
                                get_off_passengers = self.get_off(station.index, cur_time)
                                station.arrival_list.extend(get_off_passengers)
                                if self.route[self.pos]== self.route[-1]:
                                    self.terminal(cur_time, stop_time)
                                    pprint(cur_time, "Bus {} arrives at terminal {}".
                                           format(self.index, self.route[self.pos]))
                                else:
                                    station.dequeue_to_bus(cur_time, self) 

                else:
                    print('route length', len(self.route))
                    print('BUS {}, at pos {}, with route {}'.format( self.index, self.pos, self.route))
                    print('route length', len(self.route))
                    raise ValueError('not enough stations in a route')



                            
        return self.index
        
    def to_stop(self, cur_time, stop_time):
        
        time_one_run =  self.route_obj.calculate_all_dist()
        time_before_stop = stop_time - cur_time
        if time_one_run + 5 > time_before_stop:
            self.stop = True
            print("Bus {} terminates...".format(self.index))
        
        return self.stop
        