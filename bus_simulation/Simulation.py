import numpy as np
import scipy as sp
import pandas as pd

import matplotlib.pyplot as plt
import matplotlib as mpl
import sklearn
import os 
import sys
#import seaborn as sns
import random
import names

random.seed(20)

import time 
import datetime 

from .Bus import *
from .Route import *
from .Passenger import *
from .Station import *

from .Passenger import *

from .tools import *


global time_coef, passenger_dist
time_coef = 100000000
      


class Simulation:
    """
    This class defines all the simulation operations including random generate bus, 
    bus interval; generate passenger/interval randomly at each station with random
    routes, etc.
    
    Simulation world runs on a simulation time series with a time coeficent parameter
    "time_coef", which determines to how many times faster than real world. 
        
    """
    def __init__(self,
                 passenger_dist,
                 route_bus_limit= [3,3,2,2,2],
                 init_time = '08:00:00',
                 stop_time = '20:10:00',
                 bus_start_interval = 5,
                 bus_stations_index=['A', 'B', 'C', 'D', 'E', 'F'],
                 gen_bus_interval = [10, 20, 15, 10, 20],
                 dep_time = [0, 5, 10, 15, 20]
                 ):
        
        self.passenger_dist = passenger_dist
        self.init_time = init_time
        self.stop_time = stop_time
        self.bus_start_interval = 60*bus_start_interval
        
        self.bus_stations_index = bus_stations_index
        
        self.bus_stations = self.gen_bus_stations()
        self.gen_bus_interval = [i*60 for i in gen_bus_interval]#generate bus every xx minutes
        self.first_bus_dep_time = [(i*60+self.time_str_to_seconds(self.init_time)) for i in dep_time] #first bus departure time for each route        
        
        
        self.route_list = self.generate_route()
        self.route_bus_limit = route_bus_limit
        

        #
        #self.gen_passenger_interval = 60*5 #generate passengers every 60*n seconds
        #self.max_gen_passenger_number = 20 #random generate 0-n passengers
        #self.gen_passenger = True
        
        self.walk_list = []
        self.max_wt = 60*30 #
    
        self.station_check_interval = 60*15 #how long time to check station status, if people waited too long time
        
        self.arrivals = []
        


    def gen_bus_stations(self):
        bus_station_list = []
        try:
            for i in self.bus_stations_index:
                station = Station(index = i)
                bus_station_list.append(station)
            print('Bus station generated: ',[i.index for i in bus_station_list])
        except TypeError as te:
            print(self.bus_stations_index)
            raise TypeError
        return bus_station_list
    
    def arrival_list_sum(self):
        arrives = {}
        for s in self.bus_stations:
            self.arrivals.extend(s.arrival_list)
            arrives[s.index] = s.arrival_list.copy()
            s.arrival_list = []
        return arrives
    
    
    def generate_route(self):
        """generating routes following route table in class Route, i is the route number"""
        print("generating routes")
        return  [Route(i) for i in range(5)]
    
  

    def add_bus(self, route_num, cur_time):
        """
        generate bus for each route
        ------
        if (current time - last departure bus time in this station) < interval
            return False -> postpone bus departure time for 5 mins
        otherwise generate bus
        
        """
        route = self.route_list[route_num]
        
        #if route limit abort
        if  len(route.bus_list) >= route.route_bus_limit[route_num]:
            return 0
        
        for station in self.bus_stations:
            if station.index == route.route[0]:
                #check time interval betwee two buses, if TypeError, it indicates no bus come before
                #will start a bus at this station
                try:
                    if (cur_time - station.last_dep_time) < self.bus_start_interval:      
                        return -5
                
                except TypeError:
                    pprint(cur_time, 'Generating first bus at station {}, '
                           'with route{} '.format(station.index, route.route)) 
                    
                station.last_dep_time = cur_time
        
        
        
        return route.generate_bus()
    
    def time_str_to_seconds(self, time_str):
        x = time.strptime(time_str.split(',')[0],'%H:%M:%S')
        return datetime.timedelta(hours=x.tm_hour,minutes=x.tm_min,seconds=x.tm_sec).total_seconds()
    
    def time_seconds_to_str(self, cur_time):
        str_time = str(datetime.timedelta(seconds=cur_time))
        #print(str_time)
        return str_time
    
    def pprint(self,cur_time, strs, ending = False):#
        """just a print function with time"""
        if ending:
            print('[{}]: {}'.format(self.time_seconds_to_str(cur_time),strs), end = '')
        else:
            print('[{}]: {}'.format(self.time_seconds_to_str(cur_time),strs))
        
        
    def generate_passengers(self, cur_time):
        """
        generate random number of passengers at random orgin station to random destination.
        
        """
        rand_sta = 0
        rand_sta2 = 0
        flag = True
        while rand_sta == rand_sta2 and flag:
            rand_sta = random.randint(0, len(self.bus_stations_index)-1)
            rand_sta2 = random.randint(0,len(self.bus_stations_index)-1) 
            
            station1 = self.bus_stations_index[rand_sta]
            station2 = self.bus_stations_index[rand_sta2]
            
            try:
                self.route_list[0].station_dist_graph[rand_sta][rand_sta2]
                flag = False
            except:
                flag = True
            

        p = None
        for i in self.bus_stations:
            
            if i.index == self.bus_stations_index[rand_sta]:
                p = i.enqueue(cur_time, self.bus_stations_index[rand_sta2])
             
        return station1, station2

        
    def generate_passengers_with_fixed_time(self, set_time):
        """
        generate random number of passengers at random orgin station to random destination.
        
        """
        rand_sta = 0
        rand_sta2 = 0
        flag = True
        
        while rand_sta == rand_sta2 and flag:
            rand_sta = random.randint(0, len(self.bus_stations_index)-1)
            rand_sta2 = random.randint(0,len(self.bus_stations_index)-1) 
            
            station1 = self.bus_stations_index[rand_sta]
            station2 = self.bus_stations_index[rand_sta2]
            
            try:
                self.route_list[0].station_dist_graph[rand_sta][rand_sta2]
                flag = False
            except:
                flag = True
            
        p = None
        for i in self.bus_stations:
            if i.index == self.bus_stations_index[rand_sta]:
                p = i.enqueue(set_time, self.bus_stations_index[rand_sta2])
             
        return station1, station2    
    
    def generate_bus(self,tick_tock, postpone_queue):
            if len(postpone_queue)>0:
                if tick_tock % 60 ==0:
                    print("postpone queue", postpone_queue)
                postpone_queue_copy = postpone_queue.copy()
                for queue in range(len(postpone_queue)):
                    if self.add_bus(postpone_queue[queue], tick_tock) > 0:
                        r = self.route_list[postpone_queue[queue]]
                        self.pprint(tick_tock, 'Route {} a new bus {} is added from dequeue, with route {}'.
                                    format(r.route_num+1, r.bus_list[-1].index, r.route))
                        postpone_queue_copy = postpone_queue_copy[1:]
                postpone_queue = postpone_queue_copy.copy()
            
            
            #generate bus for each route
            
            for j in range(len(self.route_bus_limit)):
                if (tick_tock - self.first_bus_dep_time[j]) < 0 :
                    continue        
                    
                if (tick_tock - self.first_bus_dep_time[j])% self.gen_bus_interval[j] == 0 :
                    if self.add_bus(self.route_list[j].route_num, tick_tock) > 0:
                        self.pprint(tick_tock, 'Route {} a new bus {} is added, with route {}'.
                                    format(self.route_list[j].route_num+1, self.route_list[j].bus_list[-1].index, self.route_list[j].route))
                    elif self.add_bus(self.route_list[j].route_num, tick_tock) == -5:
                        print(self.route_list[j].route_num,"adding to queue")
                        postpone_queue.append(self.route_list[j].route_num)
                        
                        
            return postpone_queue

            
    def print_settings(self):
        print("============================================")
        print('route_bus_limit: ', self.route_bus_limit )
        print('gen_bus_interval: ', self.gen_bus_interval )
        print('max_wt: ', self.max_wt )
        print('station_check_interval: ', self.station_check_interval )
        print("============================================")

        
    def run(self):
        
        start_time = self.time_str_to_seconds(self.init_time)
        tick_tock = start_time
        stop_time = self.time_str_to_seconds(self.stop_time)

        postpone_queue = [] # postpone list for bus start at same time. i.e. 5min interval
        
        
        #generate passenger
        #passenger arrive before start-time
        early_passenger_list = self.passenger_dist[self.passenger_dist< start_time]
        print("{} passengers arrived before service time.".format(early_passenger_list.shape[0]))
        early_passenger_list = np.sort(early_passenger_list)
        for set_time in early_passenger_list:
            o,d = self.generate_passengers_with_fixed_time(set_time)
            pprint( set_time, 'Generating a passenger with origin {} and destination {} '.format(o,d))   

        #start service
        while tick_tock < stop_time:   
            
            #dequeue passengers for wait time larger than maximum waiting time
            for station in self.bus_stations:
                self.walk_list.extend(station.dequeue_to_walk(tick_tock, self.max_wt))
                
            if tick_tock % self.station_check_interval == 0 :       
                pprint(tick_tock, 'walking list length:{}'.format(len(self.walk_list)))   
        
            
            #generate passenger
            #passenger after start-time
            if tick_tock in self.passenger_dist :
                #num = random.randint(0, self.max_gen_passenger_number)
                o,d = self.generate_passengers(tick_tock)
                #pprint( tick_tock, 'Generating a passenger with origin {} and destination {} '.format(o,d))   
                
            #generate bus from queue
            postpone_queue = self.generate_bus(tick_tock, postpone_queue)
                            
                        
            #Operate bus for each route
            for route in self.route_list:
                for bus in route.bus_list:
                    bus.to_operate(tick_tock, stop_time,self.bus_stations)
                    

            #collect arrival list         
            self.arrival_list_sum()         
                    
                           
            #print('[{}]:  running..'.format(self.time_seconds_to_str(tick_tock)))
            tick_tock +=1
            time.sleep(1/time_coef) #time runs "time_coef" faster
            
        #generate passenger
        #calculate late passengers
        #passenger arrive after service time
        late_passenger_list = self.passenger_dist[self.passenger_dist >= stop_time]
        print("{} passengers arrived after service time.".format(late_passenger_list.shape[0]))
        for set_time in late_passenger_list:
            o,d = self.generate_passengers_with_fixed_time(set_time)
            pprint( set_time, 'Generating a late passenger with origin {} and destination {} '.format(o,d))   

            
            