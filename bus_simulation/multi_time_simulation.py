import numpy as np
import scipy as sp
import pandas as pd

import matplotlib.pyplot as plt
import matplotlib as mpl
import sklearn
import os 
import sys
#import seaborn as sns
import random
import names

random.seed(20)

import time 
import datetime 

from Bus import *
from Route import *
from Station import *
from Measure import *
from Passenger import *

from tools import *
from contextlib import contextmanager
@contextmanager
def suppress_stdout():
    with open(os.devnull, "w") as devnull:
        old_stdout = sys.stdout
        sys.stdout = devnull
        try:  
            yield
        finally:
            sys.stdout = old_stdout
            
def simulation_and_measure(sigma, bus_start_interval):
    avg_tt = 0
    avt_wt = 0
    print(sigma, bus_start_interval)
    passenger_dist = passenger_demand(demand=1000, sigma = sigma, start = 8*3600, end=20*3600)
    
    sim = Simulation(passenger_dist=passenger_dist, 
                    bus_start_interval=bus_start_interval,
                     gen_bus_interval = 20)
    sim.run()
    m = Measure(sim)
    
    avg_tt = m.avg_travel_time().copy()
    avt_wt = m.avg_wait_time().copy()
    #import gc
    #clean memory
    #gc.collect()

    tmp = [sigma, bus_start_interval, avg_tt, avt_wt]
    return tmp
    
    
def show_pairs(sigma, bus_start_interval):
    print(sigma, bus_start_interval)
    return [sigma, bus_start_interval] 



#surpass system prints
#
if __name__ == '__main__':
    #passgener guassian standarded deviation range from 3500 to 5000, increase by 100 each time
    sigma_list = list(range(3500, 5000, 100)) 
    #interval limitation between buses to take off, from 3 minuate to 10, increase by 1 minuate
    interval_list = list(range(3, 10, 1))

    from multiprocessing import Pool
    from itertools import product
    results = []
    p1 = Pool(20)
    with suppress_stdout() as ss:
        results.append(p1.starmap_async(simulation_and_measure, product(sigma_list, interval_list)))
    
    print(results)